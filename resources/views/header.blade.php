<?php

use Nwidart\Menus\Facades\Menu;

Menu::create('navigation', function($menu) {
    $menu->divider();

    $menu->url('/', 'Home');
    $menu->url('sports', 'Sports');
    $menu->url('subscribe', 'Subscribe');
    $menu->url('cart', 'Cart');

    $menu->divider();

    $menu->url('my-tickets', 'My Tickets <i class="far fa-user-circle"></i>');
} );

?>

    <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Lunatix Technical Assessment</title>
    <link href="{{ mix('/css/lunatix-technical-assessment.css') }}" rel="stylesheet">
</head>
    <body>
        <nav class="lunatix-nav">
            <a href="/" class="lunatix-nav__home-link">
                {{HTML::image("images/TestAsset 4.png", 'Ave Logo Home Link', array( 'class' => 'lunatix-nav__logo'))}}
            </a>
            {!! Menu::render('navigation') !!}
            <a href="#" class="lunatix-nav__mobile-toggle"><span></span></a>
        </nav>
