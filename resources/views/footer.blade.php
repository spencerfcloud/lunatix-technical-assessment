        <footer class="lunatix-footer">
            {{HTML::image(
                "images/TestAsset 24-100.jpg",
                "Download on App Store link",
                array('class' => 'lunatix-footer__download-link lunatix-footer__download-link--apple-store'))
            }}
            {{HTML::image(
                "images/TestAsset 23-100.jpg",
                "Download on Google Play link",
                array('class' => 'lunatix-footer__download-link lunatix-footer__download-link--google-play'))
            }}
            {{HTML::image(
                "images/TestAsset 12.png",
                "Newsletter Subscribe Form",
                array('class' => 'lunatix-footer__mailing-list-form'))
            }}

            <div class="lunatix-footer__text">
                <p>Powered by</p>
                {{HTML::image("images/venmo-logo.png", "Venmo Logo", array('class' => 'lunatix-footer__venmo-logo'))}}

                <p><a href="#">About</a> | <a href="#">Support</a> | <a href="#">Terms And Conditions</a></p>
                <p>Copyright © 2020 SI Tickets | All Rights Reserved</p>

                <i class="fab fa-2x fa-twitter"></i>
                <i class="fab fa-2x fa-facebook-f"></i>
                <i class="fab fa-2x fa-instagram"></i>
            </div>

            {{ HTML::script('js/app.js') }}
        </footer>
    </body>
</html>
