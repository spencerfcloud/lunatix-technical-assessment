@include('header')

<div class="lunatix-cart" style="background-image: url('{{URL::asset("images/TestAsset 30-100.jpg")}}')">
    <div class="lunatix-cart__content">
        <div class="lunatix-cart__header">
            <h3 class="lunatix-cart__title">MY CART</h3>
            <i class="far fa-times-circle fa-2x"></i>
        </div>

        <hr>

        <div class="lunatix-cart__main">
            <div class="lunatix-cart__title-venue-quantity">
                <span class="lunatix-cart__bold">EVENT 1</span>
                <p>Venue</p>
                <p>Date</p>
                <div class="lunatix-cart__quantity">
                    <span class="lunatix-cart__bold">Quantity</span>
                    {{HTML::image(
	                    "images/TestAsset 18.png",
	                    "Quantity Dropdown placeholder image",
	                    array('class' => 'lunatix-cart__quantity-dropdown-image')
                    )}}
                </div>
            </div>

            <div class="lunatix-cart__price-delete">
                <span class="lunatix-cart__bold">$1000</span>
                <i class="far fa-trash-alt fa-2x"></i>
            </div>
        </div>

        <hr>

        <div class="lunatix-cart__buttons">
            {{HTML::image("images/TestAsset 20.png", "Continue Shopping placeholder image", array('class' => 'lunatix-cart__button'))}}
            {{HTML::image("images/TestAsset 19.png", "Checkout placeholder image", array('class' => 'lunatix-cart__button'))}}
        </div>
    </div>
</div>

@include('footer')
