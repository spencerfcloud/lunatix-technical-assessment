@include('header')

<div class="lunatix-subscribe" style="background-image: url('{{URL::asset("images/TestAsset 30-100.jpg")}}')">
    <div class="lunatix-subscribe__content">
        <span class="lunatix-subscribe__title">SUBSCRIBE</span>

        <form class="lunatix-subscribe__form">
            <p class="lunatix-subscribe__name-inputs-container">
                <input class="lunatix-subscribe__form-input" type="text" placeholder="First Name">
                <input class="lunatix-subscribe__form-input" type="text" placeholder="Last Name">
            </p>
            <p><input class="lunatix-subscribe__form-input" type="text" placeholder="Email"></p>
            <p><input class="lunatix-subscribe__form-input" type="text" placeholder="User Name"></p>
            <p><input class="lunatix-subscribe__form-input" type="password" placeholder="Password"></p>
            <p><input class="lunatix-subscribe__form-input" type="password" placeholder="Repeat Password"></p>
            <p class="lunatix-subscribe__tos-privacy-policy-check">
                <input type="checkbox"> I Agree to the <a href="#">Terms of Use and Privacy Policy</a>
            </p>
            {{HTML::image(
	            "images/TestAsset 13.png",
	            "Sign Up button placeholder",
	            array('class' => 'lunatix-subscribe__form-submit'))
            }}
        </form>
    </div>
</div>

@include('footer')
