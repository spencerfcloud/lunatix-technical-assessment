@include('header')

<div class="lunatix-hero" style="background-image: url('{{URL::asset('images/TestAsset-21-100.jpg')}}');">
    <h1 class="lunatix-hero__heading">Magnanimus Tour</h1>
    {{HTML::image(
        "images/TestAsset-5.png",
        "Search Field Image",
        array( 'class' => 'lunatix-hero__search-field-image-placeholder')
    )}}
</div>

<div class="lunatix-categories-section lunatix-carousel">
    <div class="lunatix-carousel__slider">
        @for ( $image = 0; $image < 6; $image++ )
            {{HTML::image(
                "images/TestAsset 9.png",
                "Categories Placeholder Image",
                array('class' => 'lunatix-carousel__image lunatix-categories-section__image')
            )}}
        @endfor
    </div>
</div>

<hr>

<div class="lunatix-events-section">
    <h3 class="lunatix-events-section__trending-text">TRENDING</h3>
    {{HTML::image(
        "images/TestAsset 6.png",
        "Green Button Image",
        array('class' => 'lunatix-events-section__view-all-button'))
    }}

    <div class="lunatix-carousel lunatix-events-section__carousel">
        <div class="lunatix-carousel__slider">
            @for ( $image = 0; $image < 4; $image++ )
                {{HTML::image(
                    "images/TestAsset 8.png",
                    "Event Placeholder Image",
                    array('class' => 'lunatix-carousel__image lunatix-events-section__image'))
                }}
            @endfor
        </div>
    </div>
</div>

<div class="lunatix-video-section">
    <video controls class="lunatix-video">
        <source src="{{URL::asset('videos/0_CROWD_AT_OUTDOOR_FESTIVAL_FULL.mov')}}" type="video/mp4">
    </video>

    {{HTML::image("images/TestAsset 11.png", "Learn More Button Placeholder Image")}}
</div>

@include('footer');
