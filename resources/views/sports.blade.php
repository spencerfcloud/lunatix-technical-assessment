@include('header')

<div class="lunatix-hero" style="background-image: url('{{URL::asset("images/TestAsset 29-100.jpg")}}');">
    <h1 class="lunatix-hero__heading">NBA</h1>
    {{HTML::image(
        "images/TestAsset-5.png",
        "Search Field Image",
        array( 'class' => 'lunatix-hero__search-field-image-placeholder')
    )}}
</div>

<div class="lunatix-nba-teams">
    <table id="lunatix-data-table" class="lunatix-nba-teams__table"></table>
</div>

@include('footer')
