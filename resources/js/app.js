require('./bootstrap');

import $ from 'jquery';
import dataTables from 'datatables';

$(".lunatix-nav__mobile-toggle").click(function() {
    $(this).toggleClass("on");
    $(".main-mnu").slideToggle();
    return false;
});

$( '.lunatix-nav__mobile-toggle' ).click(toggleNavMenuDisplay);

function toggleNavMenuDisplay() {
    $( '.navbar-nav' ).slideToggle();
}

async function getNbaTeams() {
    const response = await fetch(
        'https://free-nba.p.rapidapi.com/teams?page=0',
        {
            method: 'GET',
            mode: 'cors',
            headers: {
                'x-rapidapi-host': 'free-nba.p.rapidapi.com',
                'x-rapidapi-key': 'e5d8563997mshcfb030c802400d3p1ec262jsn6864acf2d34a',
            }
        }
    );

    return response.json();
}

$( document ).ready(insertNbaTeams);

function insertNbaTeams() {
    if ( $('.lunatix-nba-teams' ).length > 0 ) {
        getNbaTeams().then(data => {
            console.log( data );

            var teams = data.data;

            teams = teams.map(function(object) {
                return Object.keys(object).sort().map(function(key) {
                    return object[key];
                })
            });

            console.log( teams );

            $('#lunatix-data-table').dataTable({
                data: teams,
                columns: [
                    { title: 'Abbreviation' },
                    { title: 'City' },
                    { title: 'Conference' },
                    { title: 'Division' },
                    { title: 'Team' },
                    {
                        title: 'ID',
                        visible: false,
                    },
                    {
                        title: 'Name',
                        visible: false,
                    },
                ],
                lengthMenu: [ [ 5, 10, -1 ], [ 5, 10, "All" ] ],
            });
        });
    }
}
