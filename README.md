#Lunatix Technical Assessment

This is a Laravel application for the techincal assessment of the Lunatix interview process created by Spencer Cloud.

To run this application, start docker, navigate to the root of the project using the command line, and run sail.

```./vendor/bin/sail up```

or just

```sail up```

Once the application is running, navigate to localhost in your browser to view the home page of the application.

Use the navigation menu to view each of the 4 pages.

### NBA Teams API Data

On the "Sports" page, view the pulled API data representing the NBA teams.

All data pulled from the API should be visible, sortable by each column, and you may select either 5, 10, or all teams to be shown at a time.
